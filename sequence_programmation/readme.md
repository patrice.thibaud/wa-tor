---
title: "Wator : écriture du programme Python"
lang: fr
author: P.THIBAUD
---

<!--
Création du pdf avec pandoc :  pandoc readme.md -o wator.pdf --template eisvogel --highlight-style tango
template utilisé Eisvogel : https://github.com/Wandmalfarbe/pandoc-latex-template
--->

# Wa-Tor : Ecriture du programme Python

## _PARTIE 1_ : Représentation d'une grille

Une grille du jeu  sera représentée par une liste de listes contenant une structure de donnée liée à la nature de la case : mer, thon ou requin.  
La structure choisie sera un tuple de la forme (n,g,e) ou :

* __n__ représente la nature de la case (0 pour une mer, 1 pour un thon, 2 pour un requin)
* __g__ la durée de gestation de l'espèce (0 si elle n'existe pas)
* __e__ l'énergie de l'espèce (0 si elle n'existe pas)

Par exemple la liste :  

```python
[[(0, 0,0), (0, 0, 0), (0, 0, 0)], [(1, 2, 0), (2, 5, 3), (1, 2, 0)]]
```
  
représente une grille de jeu de 6 cases, 3 cases en largeur et 2 cases en
hauteur.  
Sur la première ligne seule des mers sont présentes, 
tandis que sur la deuxième ligne : un requin est entouré par deux thons.

### Construction d'une grille

Réalisez une fonction `creer_grille` qui prend en paramètres le nombre de cases
horizontalement et celui verticalement et qui renvoie une liste de listes
correspondant à une grille aux dimensions souhaitées, ne
contenant que des mers.

```python
>>> creer_grille(3, 2)
[[(0, 0, 0), (0, 0, 0), (0, 0, 0)], [(0, 0, 0), (0, 0, 0), (0, 0, 0)]]
```

### Selection aléatoire d'une case

Afin de remplir la grille aléatoirement nous aurons besoin de sélectionner une case de cette grille.

Réaliser une fonction `selection_case` prenant en paramètres le nombre de cases horizontales
et le nombre de cases verticales et renvoyant un tuple de la forme `(x,y)` où x est l'abcisse de la case (x=0 au début d'une ligne) et 
y l'ordonnée d'une case (on commencera à y=0, y augmente de 1 lorsqu'on passe à la ligne suivante)
la case de coordonnées (0,0) sera donc la première de la grille.

![repérage des coordonnées sur la grille](../fig/representation_grille.png)

Par exemple :  

```python
>>> selection_case(3, 3)
(1, 2)
>>> selection_case(4, 4)
(3, 0)

```
  
Déclarez préalablement dans votre code deux variables globales notées `H`, `V` représentant respectivement le nombre de cases horizontales et verticales.
Vous initialiserez pour l'instant ces valeurs à 4. Elles seront utiles par la suite


### Remplissage d'une case

Le jeu nécessitera très réguliérement de permettre la naissance ou la mort de poissons,
on se propose donc de réaliser une fonction `init_case` qui prend en paramètre la nature de la case à générer (mer, thon ou requin) 
et qui retourne le tuple correspondant aux valeurs initiales des paramètres gestation et énergie.

Déclarer préalablement dans votre code trois variables globales notées `G_THON`, `G_REQUIN` et `E_REQUIN` en leur affectant les valeurs suivantes :

| Durée de gestation initiale des thons  | Durée de gestation initiale des requins |Energie initiale des requins |
| ------ | ------ |------ |
| `G_THON` | `G_REQUIN` |`E_REQUIN` |
| 2 | 5 | 3 |

Puis réalisez la fonction `init_case` qui utilisera ces variables globales

```python
>>> init_case(0)
(0, 0, 0)

>>> init_case(1)
(1, 2, 0)

>>> init_case(2)
(2, 5, 3)
```

### Placement des poissons aléatoirement dans la grille

Réaliser ensuite une fonction `placement_espece` prenant en paramètres une grille, la nature de l'espèce (thon  ou requin) à placer et le nombre de poissons.
Cette fonction retournera la grille correspondante.
Dans cette fonction vous utiliserez les fonctions précédentes `selection_case` et `init_case`.

Par exemple :  

```python
>>> placement_espece(creer_grille(2, 2), 1, 2)
[[(0, 0, 0), (1, 2, 0)], [(0, 0, 0), (1, 2, 0)]]

```
  
_AIDE_:
On pourra éventuellement utiliser l'opérateur [not in](https://docs.python.org/fr/3/reference/expressions.html#membership-test-operations) pour tester l'appartenance d'une espèce à une liste établie

### Dénombrer une espèce dans la grille

Il peut être utile de compter le nombre de thons ou de requins pour une grille donnée.
Il n'est pas simple de le faire "à la main" notamment quand celle-ci à des dimensions importantes.
Réaliser une fonction `denombre_espece` prenant en paramètre une grille et la nature de l'espèce à dénombrer.
Cette fonction retournera la nombre d'entités de cette espèce présente dans la grille.

Par exemple :  

```python
>>> grille = [[(0, 0, 0), (0, 0, 0)], [(1, 2, 0), (0, 0, 0)]]
>>> denombre_espece(grille, 1)
1
>>> denombre_espece(grille, 2)
0
```
  
### Initialisation de la grille

La création d'une grille conduisant à des évolutions intéressantes des deux populations nécessite des proportions adéquates de thon et de requin. On se propse d'utiliser des pourcentages et non pas une quantité donnée pour qhaque espèce afin d'adapter le programme à n'importe quelle taille de grille.  
Réaliser une fonction `init_grille` prenant en paramètres :

* les pourcentages initiaux pour chacune de ces espèces
* le nombre de cases horizontales et verticales de la grille

et retournant la grille correspondante.

Dans cette fonction vous utiliserez les fonctions précédentes `creer_grille`, `placement_espece` et `init_case`.  

```python
>>> init_grille(1, 0, 2, 2)
[[(1, 2, 0), (1, 2, 0)], [(1, 2, 0), (1, 2, 0)]]

>>> init_grille(0, 1, 2, 2)
[[(2, 5, 3), (2, 5, 3)], [(2, 5, 3), (2, 5, 3)]]

>>> init_grille(0.5,0.5,2,2)
[[(1, 2, 0), (1, 2, 0)], [(2, 5, 3), (2, 5, 3)]]
```  

Une surpopulation de poissons conduirait irrémédiablement à l'extinction ou dans le cas des thons à une occupation totale de la grille.  
On choisira donc des pourcentages initiaux de ces deux espèces adaptées : par exemple 30% pour les thons et 10% pour les requins.  
Déclarer préalablement dans votre code 2 variables globales notées `P_THON` et `P_REQUIN` en leur affectant les valeurs adaptées.

Testez la fonction précédente avec ces arguments d'appel.

## _PARTIE 2_ : Affichage d'une grille

La visualisation d'une grille sous la forme d'un affichage en 2 dimensions nous
permettra de mieux appréhender les déplacements des différentes espèces.

Réaliser une procédure `afficher_grille` dont le rôle sera
d'afficher de manière plus claire une grille du jeu  qui lui est
passée en paramètre. 

Les cases de mer seront affichées avec un tiret bas (`_`)
, les cases contenant un thon  seront affichées avec un T majuscule
(`T`), celles avec un requin avec un R majuscule(`R`) .
Le contenu des cases sera séparé par une espace. Chaque ligne de la grille
sera affichée sur une ligne distincte.
  
```python
>>> afficher_grille(creer_grille(3, 2))
_ _ _
_ _ _

>>> grille = [[(0, 0, 0), (0, 0, 0), (0, 0, 0)], 
              [(1, 2, 0), (2, 5, 3), (1, 2, 0)]]

>>> afficher_grille(grille)
_ _ _
T R T

```  

## _PARTIE 3_ : Gestion de l'environnement d'une case

L'interaction entre les proies et les prédateurs ainsi que les déplacements vers des mers nécessitent, lorsqu'une case a été choisie aléatoirement au début d'un tour
de jeu, de connaitre l'environnement proche.

### Trouver les cases voisines

Réaliser une fonction `cases_voisines` acceptant comme paramètres:

* les coordonnées d'une case
* le nombre de cases présentes dans la grille horizontalement
* le nombre de cases présentes dans la grille verticalement

 et retournant une liste des 4 coordonnées ( sous forme de tuple `(x, y)`) des cases voisines : une au NORD, une à l'OUEST , une à l'EST et enfin une au SUD.
Il sera nécessaire de réfléchir aux coordonnées des voisines d'une case située sur le _bord_ de la grille.

Par exemple :  

```python
>>> cases_voisines((1, 1), 2, 2)
[(1, 0), (0, 1), (0, 1), (1, 0)]

>>> cases_voisines((1, 1), 3, 3)
[(1, 0), (0, 1), (2, 1), (1, 2)]

>>> cases_voisines((2, 0), 3, 3)
[(2, -1), (1, 0), (0, 0), (2, 1)

```
  
_AIDE_:

* On peut accéder au  dernier élément d'une liste `l` ainsi : `l[-1]`
* L'opérateur modulo `%` peut s'avérer utile ici (_mais pas obligatoire_)

### Rechercher un type de case parmis ses voisines

Lors de leurs déplacements respectifs, les thons et les requins cherchent aléatoirement des cases mer ou thon parmis leurs voisins.
Réaliser une fonction `recherche_case` acceptant 3 paramètres :

* une liste des coordonnées de 4 cases
* une grille
* la nature de la case recherchée

Cette fonction retournera les coorodonnées sous forme de tuple `(x,y)` de la case_recherchée si elle est trouvée, `False` sinon.

Par exemple :  

```python
>>>grille=[[(1, 2, 0),(0, 0, 0), (0, 0, 0)], 
           [(0, 0, 0), (2, 5, 3), (0, 0, 0)]]

>>> recherche_case([(2, -1), (1, 0), (0, 0), (2, 1)], grille ,2)
False

>>> recherche_case([(1, -1), (0, 0), (2, 0), (1, 1)], grille, 1)
(0, 0)
```
  
_AIDE_:
On pourra éventuellement utiliser l'instruction [del](https://docs.python.org/fr/3/tutorial/datastructures.html#the-del-statement) 
pour supprimer les cases testées de la liste au fur et à mesure.

## _PARTIE 4_ : Actions liées aux deux espèces

### Gérer la durée de gestation

Réaliser une fonction `evol_gestation` acceptant deux paramètres : les coordonnées d'une case et une grille.  
La fonction retournera la durée de gestation de l'espèce diminuée  d'une unité

Par exemple en utilisant la même grille que precedemment :

```python
>>> evol_gestation((0, 0), grille)
1

>>> evol_gestation((1, 1), grille)
4
```
  
### Se déplacer vers une mer

Lorsque c'est le tour d'un thon il cherche à se déplacer vers une mer voisine (c'est aussi vrai pour le requin s'il n'y a pas de thon à proximité).

Réaliser une fonction `deplace_vers_mer` acceptant comme paramètres:

* la nature de l'espèce
* la case initiale sur laquelle se trouve l'espèce
* la case de mer vers laquelle l'espèce se déplace
* la grille
* la durée de gestation de l'espèce
* l'energie de l'espèce : [on pourra utiliser une valeur 0 par défaut pour ce paramètre](https://docs.python.org/fr/3/tutorial/controlflow.html#default-argument-values)

Cette fonction devra gérer les reproductions éventuelles des espèces, les réinitialisations des gestations et les cases redevenant des mers
Elle retournera la grille ayant évolué en conséquence.  

```python

>>> deplace_vers_mer(1, (0, 0), (0, 1), grille, 1)
[[(0, 0, 0), (1, 2, 0), (0, 0, 0)], [(1, 1, 0), (2, 5, 3), (0, 0, 0)]]

# ou encore avec la même grille intiale
>>> deplace_vers_mer(2, (1, 1), (2, 1), grille, 4, 2)
[[(0, 0, 0), (0, 0, 0), (0, 0, 0)], [(0, 0, 0), (0, 0, 0), (2, 4, 2)]]
```
  
_Conseil_ : il peut être très intéressant de dessiner une grille de taille raisonnable et de tester plusieurs situations pour vérifier
le comportement du poisson. Vous pourrez notamment utiliser la fonction `afficher_grille` pour réaliser ces tests.  
Pensez également à évaluer le contenu des cases pour vérifier l'évolution des paramètres du ou des poissons concernés par les modifications.

## _PARTIE 5_ : Tour du thon

Réaliser une fonction `tour_thon` qui traduira parfaitement les actions d'un thon  en retournant une grille donnant un état à la fin de ce tour.

_AIDE_ : on fera évoluer la gestation du thon en début de tour.
Puis vous utiliserez les fonctions préalablement créées, notamment :  `recherche_case` et `deplace_vers_mer`

## _PARTIE 6_ : Actions liées au requin

### Gérer l'énergie

Réaliser une fonction `evol_energie` acceptant deux paramètres : les coordonnées d'une case et une grille.
La fonction retournera l'énergie de l'espèce diminuée  d'une unité

Par exemple :  

```python

>>> evol_energie((1, 1), [[(1, 2, 0), (0, 0, 0)], [(0, 0, 0), (2, 5, 3)]])
2
```  

### Chasser un thon

Lors de son tour le requin, après perdu un temps de gestation et trouvé un thon parmis les cases voisines, mange le thon en se déplaçant sur cette case.
Réaliser une fonction `chasse_au_thon` traduisant cette dernière action.
Elle acceptera plusieurs paramètres :

* les coordonnées de la case de départ du requin
* les coordonnées de la case occupée par le thon
* la grille
* la durée de gestation

Elle retournera la grille ayant évolué en conséquence (Penser aux naissances éventuelles !)

### Tour du requin

Réaliser une fonction `tour_requin` qui traduira parfaitement les actions d'un requin en retournant une grille donnant un état à la fin de ce tour.

_AIDES_ :

* On fera évoluer la gestation et l'énergie du requin en début de tour.
* Puis vous utiliserez les fonctions préalablement créées, notamment :  `recherche_case`, `chasse_au_thon` et `deplace_vers_mer`
* Afin de ne pas rester bloquer, vous pouvez vous aider du début de l'[algorigramme proposé](./algorigramme/algorigramme_requin_debut.pdf)


## _PARTIE 7_ : Evolution des populations

### Evolution de la grille

Réaliser une fonction `evol_population` acceptant une grille comme paramètre.
Cette fonction devra simuler le comportement du jeu en choisissant tout d'abord une case aléatoirement dans la grille puis en appelant éventuellement la fonction liée aux actions de la nature de la case sélectionnée.  
Elle retournera la grille ayant évolué d'un pas de simulation

### Simulation pas à pas

Réaliser une fonction `simulation` acceptant comme paramètres :

* une grille
* un nombre de pas total de simulation
* un nombre de pas aubout du quel un affichage de la grille est réalisé.

Cette fonction utilisera la fonction `evol_population`

Si on veut avoir un affichage de l'évolution de la mer, il est peut être pertinent de ne pas afficher toutes les étapes mais une tous les 10 par exemple ( c'est l'intérêt du 3ème paramètre de la fonction `simulation`) et de faire une petite pause après chaque affichage grâce au module `time` et sa fonction `sleep` :

   ```python
   import time
   time.sleep(0.1) # pause de 0,1 seconde
   ```

_Conseil_ : Tester d'abord avec des petites tailles de grilles et un nombre de pas total raisonnable. Puis augmenter progressivement les différents paramètres, vous pourrez également diminuer le temps de pause en conséquence.

### Construction de la courbe

On pourra ajouter à la simulation des listes qui comptabilisent à chaque pas les nombres de requins et de thons.
Utilisez pour cela la fonction `denombre_espece`créée.
Ces listes peuvent être utilisées pour dessiner les courbes des évolutions de population. On peut pour cela utiliser le module `pylab`.

Tracer une courbe avec `pylab`est assez simple. Il suffit de faire :  

   ```python
   import pylab
   data_x =... # une liste d'abscisses
   data_y =... # une liste d'ordonnées
   pylab.plot(data_x, data_y)
   pylab.title('courbe des points de coordonnées (x,y)')
   pylab.show()
   ```
  
Voici alors ce que l'on peut obtenir pour une mer de taille 25x25, 125000 pas. On peut observer les cycles décalés d'évolution des populations (les thons sont en bleu et les requins en rouge) :

![exemple : Evolution populations](../fig/wator_2_3_5_25x25_200.png)

### Améliorations possibles

Vous pouvez par exemple :

* Modifiez la fonction afficher-grille afin de faire figurer les paramètres de la simulation (nbre de chaque espéces, pas de simulation...)
* Réalisez une interface graphique (avec Tkinter)

..........
