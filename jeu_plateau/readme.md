# Découverte du jeu

## Le matériel

* un [plateau de jeu](./materiel/versions_pdf/plateau.pdf) à imprimer en A3 et à plastifier
* des [pions requins et thons](./materiel/versions_pdf/pions.pdf) : 10 requins et 30 thons par groupes à plastifier
* des [cartes directions](./materiel/versions_pdf/cartes_directions.pdf) : 4 cartes par groupes (Nord, Sud, Est, Ouest) à plastifier
* une [régle du jeu](./materiel/versions_pdf/regles.pdf) par élève
* 2 dés 10 par groupe (valeurs : 0 à 9)
* un pion marqueur de tour
* un marqueur effacable par groupe

__Note__ : Les versions modifiables accessibles [ici](./materiel/versions_modifiables/). 
Le plateau, les pions, les cartes et la régle sont à plastifier)

## _Etape n°1_ :  Compréhension des régles

On distribue le matériel aux élèves ( groupes de 3 ou 4) et on leur demande de lire les régles et de tester le jeu durant 10 tours (repérés par le pion fourni). Le marqueur permet de noter sur le plateau les conditions initiales (nombre de poissons, durées de gestation et énergie des requins) ainsi que sur chaque pion les paramètres : _durée estimée 30 min_

Cette phase nous permet en passant de groupe à groupe de clarifier certains points de régles (cases voisines, utilisation du marqueur pour modifier les paramètres)

## _Etape n°2_ :  Réflexion sur la traduction des régles en fonctions élèmentaires.

Toujours par groupe on demande de proposer une liste d'actions élémentaires à prévoir pour décomposer la régles en phases simples qui se traduiront par des fonctions lors de phase de programmation. _durée estimée 15 min_

_Note_ : cette phase peut aussi se dérouler de manière collective

On propose alors quelques noms de fonctions importantes ( 4 à 5) avec une description du rôle de chacune. Chaque groupe doit fournir :

* les données nécessaires à l'utilisation de chaque fonction
* les structures de données adaptées 
* des propositions de structures programmatives pour chacune d'entre elles ( structures conditionnelles, boucles, ) 
: _durée estimée 60 min_

_Note_ : Un échange entre groupes peut être organisé pour confronter les propositions.

 A la fin de cette phase, chaque groupe devra proposer par écrit ses réponses ( on peut fournir une grille de réponse à remplir) : ce travail peut être évalué



## _Etape n°3_ :  Evaluation

On peut proposer pour finir un questionnaire pour vérifier __individuellement__ que les régles ont été bien comprises.

Par exemple : proposition de situations sur la grille, coordonnées des case voisines, etc.

Un exemple [ici](./questionnaire/questionnaire_wator.pdf)
