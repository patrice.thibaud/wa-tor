# Projet Wa-tor

Séquence pédagogique proposée pour une classe de 1ère spé NSI :

* Phase 1 : [découverte du jeu](./jeu_plateau/readme.md)
* Phase 2 : [programmation en python](./sequence_programmation/readme.md)

